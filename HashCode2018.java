import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

class Ride implements Comparable<Ride>{
    int number;
    int start_row;
    int start_column;
    int end_row;
    int end_column;
    int earliest_start;
    int latest_finish;
    int earliest_finish;
    int cost;
  
    Ride(int number, int a, int b,int  c, int d, int e, int f, int cost) {
        this.number = number;
        this.start_row = a;
        this.start_column = b;
        this.end_row = c;
        this.end_column = d;
        this.earliest_start = e;
        this.latest_finish = f;
        this.cost = cost;
        this.earliest_finish = earliest_start + cost;
      }


    @Override
    public int compareTo(Ride o) {
            if (Integer.compare(this.latest_finish, o.latest_finish) == 0) {
                return Integer.compare(this.earliest_start, o.earliest_start);    
            }
            return Integer.compare(this.latest_finish, o.latest_finish);
    }
    
    public String toString() {
    	return String.format("ride from [%d,%d] to [%d,%d], earliest start %d, latest finish %d, cost %d%n",
    			start_row, start_column, end_row, end_column, earliest_start, latest_finish, cost);
    }
    
}



class Car implements Comparable<Car> {
    int endTime = 0 ;
    int x = 0;
    int y = 0;
    int number_of_rides;
    ArrayList<Ride> rides;
    
    public Car() {
      this.endTime = 0;
      this.x = 0;
      this.y = 0;
      this.number_of_rides = 0;
      rides = new ArrayList<Ride>();
    }

   @Override
    public int compareTo(Car o) {
        return Integer.compare(this.endTime, o.endTime);
    }
    
   public String toString() {
   	return String.format("going to (%d,%d) arriving at %d%n", x, y, endTime);
   }
   public int distance(Ride r) {
     return HashCode2018.distance(x,y,r.start_row,r.end_column);
   }
}

class RideNode {
	RideNode prev;
	Ride ride;
	int endTime;
	int numberOfRides = 0;
	
	public RideNode(RideNode previousNode, Ride currentRide, int endTime){
		this.prev = previousNode;
		this.ride = currentRide;
		this.endTime = endTime;
		this.numberOfRides = prev == null ? 0 : previousNode.numberOfRides + 1;
	}
}

class W {
	RideNode[] cars;
	static int fleetSize = -1;
	Long profit = 0L;
	
 W (W previous, int carIndex, Ride r, long rideProfit, int endTime ) {
	 initializeFleet();
	 for (int i = 0; i < fleetSize && previous != null; i++) {
		 cars[i] = previous.cars[i];
	 }
	 profit = previous == null ? 0 : previous.profit;
	 if (r != null) {
		 cars[carIndex] = RideNode(cars[carIndex], r, endTime);
		 profit += rideProfit;
	 }
	}
 
 private RideNode RideNode(RideNode rideNode, Ride r, int endTime) {
	return new RideNode(rideNode, r, endTime);
 }

public String toString() {
	 return String.format("%d cars, %d profit, state: %s", cars.length, profit, cars.toString());
 }
 
 public void initializeFleet() {
	 if (fleetSize == -1) throw new RuntimeException("expecting fleetsize to be set");
	 cars = new RideNode[fleetSize];
	 for (int i = 0; i < fleetSize; i++) {
		cars[i] = new RideNode(null, null, 0);
	}
	 
 }
}

public class HashCode2018 {
	
  // Return the world with the highest profit.
  public static W highestProfitWorld(List<W> universe) {
    W output = universe.get(0); // at the start, output.profit is 0.
    for (W w: universe) {
      if (w.profit > output.profit) {
        output = w;
      }
    }
    return output;
  }

  public static int distance(int x1, int y1, int x2, int y2) {
      return (Math.abs(x1 - x2) + Math.abs(y1 - y2));
  }
  
  public static int distance(Car c, Ride r) {
    return distance(c.x,c.y,r.start_row,r.end_column);
  }

    
  public static void main(String[] args) throws IOException {
  	solve('A');
  	solve('B');
  	solve('C');
  	solve('D');
  	solve('E');
  }
  
  static void solve(char which) throws IOException {
    PrintWriter writer = new PrintWriter(new FileWriter(new File("src/io/hash2018-x.out".replace('x', which))));
    File source = new File("src/io/hash2018-x.in".replace('x',which));
		Scanner in = new Scanner(source);  

		int R = in.nextInt(); // rows
    int C = in.nextInt(); // columns
    int F = in.nextInt(); // vehicles
    int N = in.nextInt(); // rides
    int B = in.nextInt(); // bonus
    int T = in.nextInt(); // number of steps
    
    List<Ride> rides = new ArrayList<Ride>();
    W.fleetSize = F;
    

    // Read the rides and add to the collection.
    for (int i = 0; i < N; ++i) {
      int a = in.nextInt(); // start row 
      int b = in.nextInt(); // start column
      int x = in.nextInt(); // end row
      int y = in.nextInt(); // end column
      int s = in.nextInt(); // earlies start
      int f = in.nextInt(); // latest finish
      
      rides.add(new Ride(i, a, b, x, y, s, f, distance(a,b,x,y)));
    }
    in.close();
    
    Collections.sort(rides);
    ArrayList<Car> empty = new ArrayList<>();
    for(int i = 1; i <= F; i++) {
			empty.add(new Car());
    }
		
    ArrayList<W> universe = new ArrayList<>();
    for (int i = 0; i < rides.size(); i++) {
			Ride r = rides.get(i);
			W bestWorld = bestWorld(universe, r, i, B);
			universe.add(bestWorld);
    }
    
    
    

    // Pick the world with the highest profit.
    W profitableWorld = highestProfitWorld(universe);
    
    // Output the results.
    for (int i = 0; i<W.fleetSize ; i++) {
    	String r = "";
      // TODO: calculate the number of rides "number_of_rides" for car c.
       r += String.format("%d", profitableWorld.cars[i].numberOfRides);
       RideNode rn = profitableWorld.cars[i];
       while(rn.ride != null) {
      	 r+= " " + rn.ride.number;
      	 rn = rn.prev;
       }
       System.out.println(r);
       writer.println(r);
       writer.flush();
    }
    System.out.println("=======================================" + which +">"+ profitableWorld.profit);
    writer.close();
  }


	private static W bestWorld(ArrayList<W> universe, Ride r, int worldBeingBuilt, int B) {
		W justThisRide = null;
		int endTime = Math.max(distance(0, 0, r.start_row, r.start_column), r.earliest_start) + r.cost;
		if (endTime <= r.latest_finish) {
			long profit = (long) r.cost + (endTime == r.earliest_finish ? B : 0);
			justThisRide = new W(null, 0, r, profit, endTime);
		} else {
			justThisRide = new W(null, 0, null, 0, 0);
		}
		//looking through past worlds
		int best = -1;
		long maxProfitAcrossWorlds = justThisRide.profit;
		long maxRideProfitAcrossWorlds = justThisRide.profit;

		int bestCar = -1;
		int bestWorld = -1;
		int ProfitOfTheBest = 0;
		for (int j =0; j < worldBeingBuilt; j++) {
			RideNode[] fliit = universe.get(j).cars;
			int bestArrivalInThisWorld = Integer.MAX_VALUE;
			int arrival = -1;
			int bestCarInThisWorld = 0;
			for(int i=0; i<W.fleetSize;i++) {
				if (fliit[i].ride != null){
					arrival = fliit[i].endTime + distance(fliit[i].ride.end_row, fliit[i].ride.end_column, r.start_row, r.start_column) + r.cost;
				} else {
					arrival = distance(0, 0, r.start_row, r.start_column) + r.cost;
				}
				if ( arrival <= bestArrivalInThisWorld) {
					bestCarInThisWorld = i;
					bestArrivalInThisWorld = arrival;
				}
//				if (arrival <= r.earliest_start) {
//					break;
//				}
			}
			if (bestArrivalInThisWorld <= r.earliest_finish) {
				if (universe.get(j).profit + B + r.cost > maxProfitAcrossWorlds) {
					maxProfitAcrossWorlds = universe.get(j).profit + B + r.cost;
					maxRideProfitAcrossWorlds = B + r.cost;
					best = j;
					bestWorld = j;
					bestCar = bestCarInThisWorld;
				}
			} else if (bestArrivalInThisWorld <= r.latest_finish) {
				if (universe.get(j).profit + r.cost > maxProfitAcrossWorlds) {
					maxProfitAcrossWorlds = universe.get(j).profit + r.cost;
					maxProfitAcrossWorlds = r.cost;
					best = j;
					bestWorld = j;
					bestCar = bestCarInThisWorld;
				}
			} 
//			else {
//				if (universe.get(j).profit  > maxProfitAcrossWorlds) {
//					maxProfitAcrossWorlds =universe.get(j).profit;
//					best = j;
//					bestWorld = j;
//					System.out.printf("dropping ride %d : %s earliest finish: %d\n" , r.number, r.toString(), arrival);
//					// no ride
//				}
//			}
			
			
		}
		
		if (best == -1 ) {
			return justThisRide;
		} else {
			//create copy and add ride
			W winnerWorld = new W(universe.get(best), bestCar, r, maxProfitAcrossWorlds-universe.get(best).profit, (int) maxRideProfitAcrossWorlds);
			winnerWorld.profit = maxProfitAcrossWorlds;
			return winnerWorld;
			
		}
	}
  
}